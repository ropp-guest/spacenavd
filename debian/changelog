spacenavd (0.7.1-1) unstable; urgency=medium

  * Make the build reproducible (Thanks Reiner Herrmann). Closes: #843097.
  * Update Standards-Version to 3.9.8.
  * Add missing lsb-base dependency.
  * New upstream version 0.7.1. Closes: #959833.
  * Fix FTBFS with GCC10. Refs: #957827.
    - Fixed upstream in 081182f1675bb69eec58d92698f1ba2f23466d3f
  * Upgrade patches for 0.7.1 release.
  * Remove dh-systemd dependency. Closes: #958617.
  * Update docs and compat files.
  * Bump standards version to 4.5.0.
  * Move Vcs-* fields to point salsa page.
  * Add documentation field to systemd service file.
  * Use https for copyright-format (insecure-copyright-format-uri).

 -- Rodolphe Pelloux-Prayer <rodolphe@damsy.net>  Wed, 19 Aug 2020 21:14:29 +0200

spacenavd (0.6-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "conflict with /dev/input/js0" (Closes: #916610)
    - Fixed upstream in 34ddda1246ad07e8ff2e6606224e710852e3e3d8

 -- Jakob Haufe <sur5r@sur5r.net>  Sat, 01 Jun 2019 11:13:33 +0000

spacenavd (0.6-1) unstable; urgency=medium

  * Imported Upstream version 0.6
  * Remove initfile patch. We use debian/spacenavd.init anyway.
  * Rewrite init script to use start-stop-daemon.
  * Remove makefile patch.
  * Use DEP-5 copyright format.
  * Add missing copyright for src/serial and src/magellan/*.
  * Add upstream example config file.
  * Enable systemd service file.
  * Switch to debhelper > 9.
  * Add Vcs-* fields.
  * Remove quilt and autotools-dev dependencies.
  * Bump standards version to 3.9.6 and remove obsolete DM-Upload-Allowed field.
  * Set architecture linux-any. Closes: #745177.
  * Patch to use /run instead of /var/run.
  * Enable hardenings flags and patch Makefile.
  * New maintainer. Closes: #781184.

 -- Rodolphe PELLOUX-PRAYER <rodolphe@damsy.net>  Sun, 17 May 2015 12:56:37 +0200

spacenavd (0.5-1) unstable; urgency=low

  * New upstream.

 -- M G Berberich <berberic@fmi.uni-passau.de>  Tue, 16 Nov 2010 22:57:21 +0100

spacenavd (0.4-2) unstable; urgency=low

  * Put daemon in /usr/sbin (Closes: #591437).
  * Fixed typo in control-file (Closes: #589663).

 -- M G Berberich <berberic@fmi.uni-passau.de>  Tue, 03 Aug 2010 22:38:27 +0200

spacenavd (0.4-1) unstable; urgency=low

  * Initial Debian upload (Closes: #552380).
  * New upstream release

 -- M G Berberich <berberic@fmi.uni-passau.de>  Sun, 25 Oct 2009 21:22:23 +0100
